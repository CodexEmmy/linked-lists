#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

int main()
{
    struct Node *head = NULL;
    int choice, data;
    while (1)
    {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Delete\n");
        printf("4. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        if (choice == 1)
        {
            printList(head);
            break;
        }
        else if (choice == 2)
        {
            int appendChoice;
            printf("Append:\n");
            printf("1. Last Position\n");
            printf("2. After Key\n");
            printf("3. After Value\n");
            printf("Enter your choice: ");
            scanf("%d", &appendChoice);

            if (appendChoice == 1)
            {
                int num;
                printf("Enter data to append: ");
                scanf("%d", &num);
                append(&head, num);
            }
            else if (appendChoice == 2)
            {
                int key, value;
                printf("Enter key after which to append: ");
                scanf("%d", &key);
                printf("Enter value to append: ");
                scanf("%d", &value);
                insertAfterKey(&head, key, value);
            }
            else if (appendChoice == 3)
            {
                int searchValue, newValue;
                printf("Enter value after which to append: ");
                scanf("%d", &searchValue);
                printf("Enter new value to append: ");
                scanf("%d", &newValue);
                insertAfterValue(&head, searchValue, newValue);
            }
            else
            {
                printf("Invalid choice \n");
            }
        }
        else if (choice == 3)
        {
            int deleteChoice;
            printf("Delete by:\n");
            printf("1. Key\n");
            printf("2. Value\n");
            printf("Enter your choice: ");
            scanf("%d", &deleteChoice);

            if (deleteChoice == 1)
            {
                int key;
                printf("Enter key to delete: ");
                scanf("%d", &key);
                deleteByKey(&head, key);
            }
            if (deleteChoice == 2)
            {
                int value;
                printf("Enter value to delete from: ");
                scanf("%d", &value);
                deleteByValue(&head, value);
            }
            else
            {
                printf("Invalid choice \n");
            }
        }
        else if (choice == 4)
        {
            printf("Exiting.\n");
            return 0;
        }
        else
        {
            printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}
struct Node *createNode(int num){
    struct Node *temp;
    temp = (struct Node *)malloc (sizeof (struct Node)); //allocate memory
    temp->number = num; //give new node value of num
    temp->next = NULL; //there's no next node
    return temp;
}
void printList(struct Node *head){
    int counter = 0; //counter is used to show the position of the printed element
    struct Node *temp = head;
    while(temp != NULL){
        printf("The number of %d element is: %d \n",counter,temp->number);
        counter += 1;
        temp = temp->next; //go to the next node
    }
}
void append(struct Node **head, int num){
    struct Node *temp;
    temp = createNode(num); //create a new node and give its address to temp
    if (*head == NULL) //list is empty
    {
        *head = temp; //the new node will be the first and only node
    }
    else{
        struct Node *temp_next;
        temp_next = *head;
        while(temp_next->next != NULL) //use temp_next to iterate the list
        {
            temp_next = temp_next->next;
        }
        temp_next->next = temp; //temp_next held the last element, now points to new node
    }
    printf("Success! \n");
}
void prepend(struct Node **head, int num){
    struct Node *temp;
    temp = createNode(num); //temporary pointer holds new node address
    temp->next = *head; //the new node's next is the initial list head
    *head = temp;
}
void deleteByKey(struct Node **head, int key){
    struct Node *temp = *head;
    struct Node *prev = NULL;
    int count = 0;
    if ((*head) == NULL) //check if the list is empty and return
    {
        printf("List is Empty \n");
        return;
    }
    else
    {
        //prev = temp;
        while (temp != NULL)
        {
            if (count == key)
            {
                if (count == 0) //if we're removing first node
                {
                    (*head) = temp->next; //head pointer now holds the next node
                    free(temp); //free the deleted node's memory
                    printf("Node successfully removed \n");
                    return;
                }
                else
                {
                    prev = temp;
                }
                prev->next = temp->next; //prev node is linked to the new next node
                free(temp); //free the middle node corresponding to KEy
                printf("Node removed successfully! \n");
                return;
            }
            else
            {
                prev = temp;
                temp = temp->next;
                count += 1;
            }
        }
    }
}
void deleteByValue(struct Node **head, int value)
{
    struct Node *temp = *head;
    struct Node *prev = NULL;
    if ((*head) == NULL) //check if the list is empty and return
    {
        printf("List is Empty \n");
        return;
    }
    else
    {
        //prev = temp;
        while (temp != NULL)
        {
            if (value == temp->number)
            {
                if (prev == NULL) //if we're removing first node as *prev is empty
                {
                    (*head) = temp->next; //head pointer now holds the next node
                    free(temp); //free the deleted node's memory
                    printf("Node successfully removed \n");
                    return;
                }
                else
                {
                    prev = temp;
                }
                prev->next = temp->next; //prev node is linked to the new next node
                free(temp); //free the middle node corresponding to KEy
                printf("Node removed successfully! \n");
                return;
            }
            else
            {
                prev = temp;
                temp = temp->next;
            }
        }
    }
}
void insertAfterKey(struct Node **head, int key, int value)
{
    struct Node *temp = *head;
    struct Node *prev = NULL;
    struct Node *temp2 = NULL;
    int count = 0;
    if ((*head) == NULL) //check if the list is empty and return
    {
        printf("List is Empty \n");
        return;
    }
    else
    {
        //prev = temp;
        while (temp != NULL)
        {
            if (count == key)
            {
                if (count == 0) //if we're inserting at the first node
                {
                    prepend(*head, value); //head pointer now holds the new node
                    printf("Node added successfully! \n");
                    return;
                }
                else
                {
                    prev = temp;
                    temp2 = prev; //temp2 is used to interchange position of 3 nodes
                    temp = createNode(value);
                }
                prev->next = temp; //prev node is linked to the new next node
                temp->next = temp2;
                printf("Node added successfully! \n");
                return;
            }
            else
            {
                prev = temp;
                temp = temp->next;
                count += 1;
            }
        }
    }
}
void insertAfterValue(struct Node **head, int searchValue, int newValue)
{
    struct Node *temp = *head;
    struct Node *prev = NULL;
    struct Node *temp2 = NULL;
    if ((*head) == NULL) //check if the list is empty and return
    {
        printf("List is Empty \n");
        return;
    }
    else
    {
        //prev = temp;
        while (temp != NULL)
        {
            if (searchValue == temp->number)
            {
                if (prev == NULL) // prev is null if we're inserting at the first node
                {
                    prepend(*head, newValue); //head pointer now holds the new node
                    printf("Node added successfully! \n");
                    return;
                }
                else
                {
                    prev = temp;
                    temp2 = prev; //temp2 is used to interchange position of 3 nodes
                    temp = createNode(newValue);
                }
                prev->next = temp; //prev node is linked to the new next node
                temp->next = temp2;
                printf("Node added successfully! \n");
                return;
            }
            else
            {
                prev = temp;
                temp = temp->next;
            }
        }
    }
}

